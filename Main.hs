{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fplugin=Polysemy.Plugin #-}

module Main where

import Composite.Aeson
import Composite.Dhall ()
import Composite.Record
import Composite.TH
import qualified Control.Lens as L
import qualified Data.Map as Map
import Data.Monoid.Generic
import qualified Data.Text as T
import Polysemy.FSKVStore
import Polysemy.Path
import Techlab
import qualified Techlab.Dhall as D
import Text.Compdoc (Compdoc (..), FContent, fContent)
import Text.Compdoc.Dhall (compdocDecoder)
import Text.Pandoc (def)

type ProjectRoute = Title -> Url

type Title = Text

type Url = Text

type Content = Text

withLensesAndProxies
  [d|
    type FId = "id" :-> Text

    type FTitle = "title" :-> Text

    type FTags = "tags" :-> [Text]

    type FUpdated = "updated" :-> UTCTime

    type FUrl = "url" :-> Text
    |]

type BasicFields = FId : FTitle : FTags : FUpdated : FContent : '[]

basicFields :: Rec (JsonField e) BasicFields
basicFields =
  (field textJsonFormat :: JsonField e FId)
    :& (field textJsonFormat :: JsonField e FTitle)
    :& (field (listJsonFormat textJsonFormat) :: JsonField e FTags)
    :& (field iso8601DateTimeJsonFormat :: JsonField e FUpdated)
    :& (field textJsonFormat :: JsonField e FContent)
    :& RNil

type ProjectTemplate = Title -> Content -> Text

type ProjectMeta = '[FId, FTitle, FTags, FUpdated]

type Project = Compdoc ProjectMeta

type ProjectLink = Record '[FUrl, FId, FTitle]

instance D.FromDhall Project where
  autoWith = compdocDecoder def (recordJsonFormat (rcast @ProjectMeta basicFields))

type ProjectsTemplate = [ProjectLink] -> Text

data Templates = Templates
  { _index :: Text,
    _post :: Title -> Content -> Text,
    _project :: ProjectTemplate,
    _projects :: ProjectsTemplate
  }
  deriving stock (Generic)
  deriving
    (D.FromDhall)
    via D.Codec (D.Field (D.DropPrefix "_")) Templates

makeFieldsNoPrefix ''Templates

data Routes = Routes
  { _projects :: ProjectRoute
  }
  deriving stock (Generic)
  deriving
    (D.FromDhall)
    via D.Codec (D.Field (D.DropPrefix "_")) Routes

makeFieldsNoPrefix ''Routes

type IndexPage = Text

type ProjectsPage = Text

data Techbook = Techbook
  { _projects :: [Project],
    _templates :: Templates,
    _routes :: Routes,
    _verbatim :: [Path Rel File]
  }
  deriving stock (Generic)
  deriving
    (D.FromDhall)
    via D.Codec (D.Field (D.DropPrefix "_")) Techbook

makeFieldsNoPrefix ''Techbook

data Site = Site
  { _routes :: Techlab.Map Url Text
  }
  deriving (Eq, Show, Generic)
  deriving (Semigroup) via GenericSemigroup Site
  deriving (Monoid) via GenericMonoid Site

makeFieldsNoPrefix ''Site

newtype Verbatim = Verbatim ByteString

homotopictech ::
  Members
    '[ Input Techbook,
       Tagged "public" (Input (Path Rel Dir)),
       Tagged "site" (Input (Path Rel Dir)),
       FSCopy,
       FSDir,
       Output Site,
       Methodology Techbook Site
     ]
    r =>
  Sem r ()
homotopictech =
  input @Techbook >>= \x -> do
    k <- process @Techbook @Site x
    let xs = view verbatim x
    i <- tag @"public" input
    i' <- tag @"site" input
    forM_ xs $ \a -> do
      createDirectory (parent (i </> a))
      copyFile (i' </> a) (i </> a)
    output @Site k

fromGroundedUrlF :: Members '[Error PathException] r => Url -> Sem r (Path Rel File)
fromGroundedUrlF x = (parseAbsFile . T.unpack $ x) >>= stripProperPrefix $(mkAbsDir "/")

buildProjectPage :: ProjectRoute -> ProjectTemplate -> Project -> Site
buildProjectPage k f (Compdoc x) = Site . Map.singleton (k $ L.view fId x) $ f (L.view fTitle x) (L.view fContent x)

toProjectLink :: ProjectRoute -> Project -> ProjectLink
toProjectLink f (Compdoc x) = val @"url" (f $ L.view fId x) :& rcast x

buildProjectsPage :: ProjectsTemplate -> ProjectRoute -> [Project] -> Site
buildProjectsPage g f xs = Site . Map.singleton ("/projects/index.html") $ g $ map (toProjectLink f) xs

main :: IO ()
main = do
  x <- D.input D.auto "./site/index.dhall"
  homotopictech
    & runInputConst @Techbook x
    & mapOutput (view routes)
    & runOutputMapAsKVStore @Url @Text
    & runKVStoreAsKVStoreSem @Url @Text @(Path Rel File) @Text fromGroundedUrlF return return
    & runFSKVStoreRelUtf8 $(mkRelDir "public")
    & runMethodologyMappendPure @Techbook @Site
      (\x' -> Site [("/index.html", view (templates % index) x')])
    & cutMethodology @Techbook @[Project] @Site
    & runMethodologyPure @Techbook @[Project] (view projects)
    & runMethodologyMappendPure @[Project] @Site
      (buildProjectsPage (view (templates % projects) x) (view (routes % projects) x))
    & mconcatMethodology @[] @Project @Site
    & runMethodologyPure @Project @Site
      (buildProjectPage (view (routes % projects) x) (view (templates % project) x))
    & untag @"site"
    & runInputConst $(mkRelDir "site")
    & untag @"public"
    & runInputConst $(mkRelDir "public")
    & runFSCopy
    & runFSDir
    & runError @PathException
    & runM
    & (>>= either print return)
