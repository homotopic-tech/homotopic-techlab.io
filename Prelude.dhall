let Attr = { mapKey : Text, mapValue : Text }

let Prelude = https://prelude.dhall-lang.org/package.dhall

let XML = Prelude.XML

let Rel =
      < Alternate
      | Author
      | DNSPrefetch
      | Help
      | Icon
      | License
      | Next
      | Pingback
      | Preconnect
      | Prefetch
      | Preload
      | Prerender
      | Prev
      | Search
      | Stylesheet
      >

let CrossOrigin = < Anonymous | UseCredentials >

let Link = { Type =
  { rel       : Rel
  , href      : Text
  , type      : Text
  , integrity : Optional Text
  , crossorigin : Optional CrossOrigin }
  , default = { integrity = None }
}

let Meta = { name : Text, content : Text }

let Script = { src : Text
             , type : Text
             , integrity : Optional Text
             , crossorigin : Optional CrossOrigin}

let link = λ(attributes : List Attr) → XML.leaf { name = "link", attributes }

let meta = \(attributes : List Attr) -> XML.leaf { name = "meta", attributes }

let title = \(attributes : List Attr) -> \(content : List XML.Type) -> XML.element { name = "title", attributes, content }

let head = \(attributes : List Attr) -> \(content : List XML.Type) -> XML.element { name = "head", attributes, content }

let script = \(attributes : List Attr) -> XML.element { name = "script", attributes, content = ([XML.text " "] : List XML.Type) }

let renderCrossOrigin
    : CrossOrigin → Text
    = λ(x : CrossOrigin) →
        merge { Anonymous = "anonymous", UseCredentials = "use-credentials" } x

let renderRel
    : Rel → Text
    = λ(x : Rel) →
        merge
          { Alternate = "alternate"
          , Author = "author"
          , DNSPrefetch = "dns-prefetch"
          , Help = "help"
          , Icon = "icon"
          , License = "license"
          , Next = "next"
          , Pingback = "pingback"
          , Preconnect = "preconnect"
          , Prefetch = "prefetch"
          , Preload = "preload"
          , Prerender = "prerender"
          , Prev = "prev"
          , Search = "search"
          , Stylesheet = "stylesheet"
          }
          x
let rel = XML.attribute "rel"

let crossorigin = XML.attribute "crossorigin"

let type = XML.attribute "type"

let src = XML.attribute "src"

let href = XML.attribute "href"

let integrity = XML.attribute "integrity"

let toRelAttr : Rel -> Attr = \(x: Rel) -> rel (renderRel x)

let toCrossOriginAttr
    : CrossOrigin → Attr
    = λ(x : CrossOrigin) → crossorigin (renderCrossOrigin x)

let catOptionals
    : ∀(a : Type) → List (Optional a) → List a
    = λ(a : Type) →
      λ(xs : List (Optional a)) →
        Prelude.List.concatMap (Optional a) a (Prelude.Optional.toList a) xs

let toLinkAttrs : Link.Type -> List Attr = \(x : Link.Type) -> catOptionals Attr [
  Some (toRelAttr x.rel),
  Some (href x.href),
  Some (type x.type),
  Prelude.Optional.map Text Attr integrity x.integrity,
  Prelude.Optional.map CrossOrigin Attr toCrossOriginAttr x.crossorigin]

let toLinkLeaf : Link.Type -> XML.Type = \(x : Link.Type) -> link (toLinkAttrs x)

let toMetaAttrs : Meta -> List Attr = \(x : Meta) -> [XML.attribute "name" x.name, XML.attribute "content" x.content]

let toMetaLeaf : Meta -> XML.Type = \(x : Meta) -> meta (toMetaAttrs x)

let toScriptAttrs : Script -> List Attr = \(x : Script) -> catOptionals Attr [
  Some (src x.src),
  Some (type x.type),
  Prelude.Optional.map Text Attr integrity x.integrity,
  Prelude.Optional.map CrossOrigin Attr toCrossOriginAttr x.crossorigin]

let toScriptLeaf : Script -> XML.Type = \(x : Script) -> script (toScriptAttrs x)

let HTML = { link, title, meta, Meta, Script, Rel, CrossOrigin, Link, renderRel, renderCrossOrigin, toRelAttr, toCrossOriginAttr, toLinkAttrs, toLinkLeaf, toMetaAttrs, toMetaLeaf, head, toScriptLeaf} // XML

in  Prelude ⫽ { HTML, catOptionals }
