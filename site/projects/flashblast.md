---
id: flashblast
title: Flashblast - A tool for generating language learning flashcards.
tags: [anki, dhall, polysemy]
posted: 2020-10-13
updated: 2020-10-13T13:00:00Z
---

Acquiring a new language is a practice that is frequently approached
incorrectly. Many apps for learning languages take advantage of how easy
it is to mislead people into believing they are making progress. These apps
will assail the user with translation and decision exercises which fail to
instil the understanding required to allow one to speak and think in their
target language. Essentially, these apps are targetting the *wrong part of
the brain*.

Modern correct methods (such as 'Fluent Forever'), use principles from
neuroscience to provide a framework for language learning that centers around
invoking the visual and emotional centres of the brain to do all of the heavy
lifting. By both using stories that are personally relevant and making
personalised flashcards from scratch, the user can acquire vocabulary and
grammar in a much more natural setting with a higher retention rate.

Of course, finding large bodies of comprehensible input, and coming up with
emotionally relevant images to accompany them is a huge task, but we do
not want to cheat by using premade decks - tussling
with the material and creating ones own flashcards is a crucial part of the
effectiveness of the method. Flashcard GUIs are also generally a chore to use
and lack declarative ways to specify cards.

We believe we can find an optimal spot amongst these requirements by creating a
tool that is declarative and can automatically produce huge quantities of
emotionally relevant flashcards without sacrificing the effectiveness of
personalisation.

By using video media that the user is already familiar with (tv shows, films,
games), and using a subtitle track that has been self-checked or verified by a
native speaker, the user may then engage with the material by selecting words
and phrases that he or she may want to learn, and then automatically extract
all of the relevant audio accompanied by stills and produce a flashcard deck.
The result is a deck containing potentially hundreds of flashcards with familiar visual
context, clear correct audio and custom personalisation.

The application we have designed is a command line application called
[flashblast](https://gitlab.com/homotopic-tech/flashblast). Using a
[dhall](https://dhall-lang.org) based config, the user can specify a local
video file or a file from YouTube and provide a subtitle track with the cloze
deletions they wish to practice. In the [sample
deck](https://gitlab.com/homotopic-tech/flashblast-sample-decks), we
demonstrate using two of the movies from YouTube provided by the blender
foundation, "Cosmos Laundromat" and "Sintel".

The first step is to prepare a subtitle track with the correct timestamps and
personalised cloze deletions in .srt format.

```
11
00:02:28,850 --> 00:02:31,750
A {{dangerous}} {{quest}} {{for}} a lone {{hunter}}.

12
00:02:32,950 --> 00:02:35,870
I've been {{alone}} for {{as long as}} I can {{remember}}.
```

Where the {{ }} represent words or phrases that the student would like to test
themselves as being able to produce on command.

Using a dhall config, we can specify the url, output files, and subtitle track.

```{.dhall}
  { source =
      VideoSource.YouTubeDL
        { url = "https://www.youtube.com/watch?v=eRsGyueVLvQ"
        , out = "Sintel.mp4"
        , format = "mp4"
        }
  , subs = ./"Sintel.srt" as Text
  , clipf = λ(n : Text) → "Sintel-${n}.mkv"
  , audiof = λ(n : Text) → "Sintel-${n}.mp3"
  , framef = λ(n : Text) → "Sintel-${n}.bmp"
  }
```

This expression forms part of a deck, the config of which can be found in the
sample deck.

Running the generator and importing the results into anki will produce cards
with cloze deletions and an image prompt on the front, and an audio reveal on
the back.

![](/img/flashblast-sample-1.png)

Hearing the audio several times with visual context according to a spaced
repetition algorithm will give a much greater chance for the material to enter
long term memory.

We hope you check back as we continue to develop this tool.
