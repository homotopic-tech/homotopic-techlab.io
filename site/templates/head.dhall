let Prelude = ../../Prelude.dhall

let HTML = Prelude.HTML

in  \(x : ./MetaBundle) ->
    \(title : Text) ->
      HTML.head
        [ HTML.attribute "profile" "http://www.w3.org/2005/10/profile" ]
        (   [ HTML.meta [ HTML.attribute "charset" "UTF-8" ] ]
          # [ HTML.title HTML.emptyAttributes [ HTML.text title ] ]
          # Prelude.List.map HTML.Meta HTML.Type HTML.toMetaLeaf x.metas
          # Prelude.List.map HTML.Link.Type HTML.Type HTML.toLinkLeaf x.linkrels
          # Prelude.List.map HTML.Script HTML.Type HTML.toScriptLeaf x.scripts
        )
