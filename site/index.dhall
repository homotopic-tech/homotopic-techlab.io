let Prelude = ../Prelude.dhall

let HTML = Prelude.HTML

let Rel = Prelude.HTML.Rel

let CrossOrigin = Prelude.HTML.CrossOrigin

let copyright = "COPYRIGHT DANIEL FIRTH 2020"

let social =
      [ { id = "twitter", url = "https://twitter.com/homotopictech" }
      , { id = "gitlab", url = "https://gitlab.com/homotopic-tech" }
      ]

let metas =
      [ { name = "viewport", content = "width=device-width, initial-scale=1" } ]

let jsscript =
      λ(u : Text) →
      λ(i : Optional Text) →
        { type = "text/javascript", src = u, integrity = i, crossorigin = Some CrossOrigin.Anonymous }

let nav = [ { url = "/", id = "Home" }
          , { url = "/#about", id = "About" }
          , { url = "/projects", id = "Projects" }
          , { url = "/#contact", id = "Contact" }
          ]
let scripts =
      [
      jsscript
          "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
          ( Some
              "sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
          )
      , jsscript
          "https://cdn.jsdelivr.net/npm/sudoslider@3.5.0/js/jquery.sudoSlider.min.js"
          (Some "sha256-jhgyTeC6IL/fR5R777vflEtlTg9lkxcPjfTVKC0AK2c=")
      , jsscript "/js/script.js" (None Text)
      ]

let stylesheet =
      λ(u : Text) →
      λ(i : Optional Text) →
        { rel = Rel.Stylesheet, type = "text/css", href = u, integrity = i, crossorigin=(Some CrossOrigin.Anonymous) }

let linkrels =
      [ stylesheet
          "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          ( Some
              "sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          )
      , stylesheet "https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" (None Text)
      , stylesheet "/css/style.css" (None Text)
      , stylesheet "/css/media.css" (None Text)
      , stylesheet "/css/highlight.css" (None Text)
      , stylesheet "https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,600italic,400italic,800,700" (None Text)
      , stylesheet "https://fonts.googleapis.com/css?family=Oswald:400,700,300" (None Text)
      ]

let metabundle = { metas, scripts, linkrels } : ./templates/MetaBundle

let Templates =
      { index : Text, post : Text → Text → Text, project : Text → Text → Text , projects : List ./templates/Project -> Text}

let Routes = { projects : Text → Text }

in  { projects = [ ./projects/flashblast.md as Text, ./projects/polysemy-methodology.md as Text ]
    , templates =
          { index =
              ./templates/index
                metabundle
                nav
                social
                copyright
                "Homotopic.Tech - Modern Software Solution With Functional Programming."
          , post =
              ./templates/post
                metabundle
                nav
                social
                copyright
          , project =
              ./templates/post
                metabundle
                nav
                social
                copyright
          , projects =
              ./templates/projects
                metabundle
                nav
                social
                copyright
                "Projects"
          }
        : Templates
    , routes.projects = λ(x : Text) → "/projects/${x}.html"
    , verbatim =
      [ "css/media.css"
      , "css/style.css"
      , "css/highlight.css"
      , "js/script.js"
      , "webfonts/FontAwesome.otf"
      , "webfonts/fontawesome-webfont.eot"
      , "webfonts/fontawesome-webfont.svg"
      , "webfonts/fontawesome-webfont.ttf"
      , "webfonts/fontawesome-webfont.woff"
      , "webfonts/fontawesome-webfont.woff2"
      , "img/flashblast-sample-1.png"
      , "img/logo.png"
      , "img/out.jpg"
      , "img/out2.jpg"
      , "img/desgin-bg.jpg"
      , "img/Development-bg.jpg"
      , "img/concept-bg.jpg"
      , "img/systam-bg.jpg"
      , "img/desgin-icn.png"
      , "img/Development-cn.png"
      , "img/concept-icn.png"
      , "img/systam-icn.png"
      , "img/Shadow-img.png"
      , "img/Banner-bg.jpg"
      , "img/Services-bg.jpg"
      , "img/Contact-bg.jpg"
      , "img/Get-bg.jpg"
      , "img/prev-arrow.png"
      , "img/next-arrow.png"
      , "img/GitHub-Mark.png"
      , "img/Github.png"
      , "img/gitlab-icon-rgb.png"
      ]
    }
