#!/usr/bin/env bash
nix-shell \
  --substituters \
    "https://cache.nixos.org \
     https://shakebook.cachix.org \
     https://iohk.cachix.org \
     https://hydra.iohk.io" \
  --trusted-public-keys \
    "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= \
     shakebook.cachix.org-1:y42OvYap/3mM35QNli/aqCr8PuxkWYZg+zjvhyz+Enc= \
     iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo= \
     hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" \
  --command 'runhaskell Main.hs'
