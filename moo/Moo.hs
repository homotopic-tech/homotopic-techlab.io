
import Moo.GeneticAlgorithm.Binary
import Moo.GeneticAlgorithm.Continuous
import Moo.GeneticAlgorithm.Constraints
import Moo.GeneticAlgorithm.Multiobjective
import Moo.GeneticAlgorithm.Statistics

import RIO
import Control.Monad (liftM, when)
import Data.List (intercalate)
import System.Console.GetOpt
import System.Environment (getArgs, getProgName)
import Text.Printf


data Flag = RunGenerations Int
          | PrintBest Bool
          | PrintStats Bool
          | DumpAll Bool
          | ShowHelp
            deriving (Show, Eq)


data ExampleDefaults = ExampleDefaults
    { numGenerations :: Int
    , printBest :: Bool
    , printStats :: Bool
    , dumpAll :: Bool
    } deriving (Show, Eq)


exampleDefaults = ExampleDefaults {
                    numGenerations = 100
                  , printBest = True
                  , printStats = False
                  , dumpAll = False
                  }


exampleOptions :: ExampleDefaults -> [OptDescr Flag]
exampleOptions c =
    [ Option "gn" ["generations"]
                 (ReqArg (RunGenerations . read) "N")
                 ("number of generations (default: " ++ show (numGenerations c) ++ ")")
    , Option "b" ["best"]
                 (NoArg $ PrintBest True)
                 ("print the best solution" ++ (isDefault (printBest c)))
    , Option ""  ["no-best"]
                 (NoArg $ PrintBest False)
                 ("don't print the best solution" ++ (isDefault (not . printBest $ c)))
    , Option "d" ["dump"]
                 (NoArg $ DumpAll True)
                 ("dump the entire population and its objective values" ++ isDefault (dumpAll c))
    , Option ""  ["no-dump"]
                 (NoArg $ DumpAll False)
                 ("don't dump the entire population" ++ isDefault (not . dumpAll $ c))
    , Option "s" ["stats"]
                 (NoArg $ PrintStats True)
                 ("print population statistics" ++ isDefault (printStats c))
    , Option ""  ["no-stats"]
                 (NoArg $ PrintStats False)
                 ("don't print population statistics" ++ isDefault (not . printStats $ c))
    , Option "h" ["help"]
                 (NoArg ShowHelp)
                 "show help"
    ]
   where
   isDefault :: Bool -> String
   isDefault True = " (default)"
   isDefault False = ""


updateDefaults :: ExampleDefaults -> [Flag] -> ExampleDefaults
updateDefaults d (RunGenerations n:opts) = updateDefaults (d { numGenerations = n }) opts
updateDefaults d (PrintBest b:opts) = updateDefaults (d { printBest = b }) opts
-- --stats overrid --dump, and vice versa
updateDefaults d (DumpAll b:opts) =
    let ps = printStats d
    in  flip updateDefaults opts (d { dumpAll = b, printStats = ps && (not b)})
updateDefaults d (PrintStats b:opts) =
    let da = dumpAll d
    in  flip updateDefaults opts (d { printStats = b, dumpAll = da && (not b)})
updateDefaults d [] = d



printHeader conf = do
  when (printStats conf) $ putStrLn "# best, median"
  when (dumpAll conf) $ putStrLn "# x1, x2, ..., objective1, objective2, ..."


printSnapshot conf sorted = do
  when (printBest conf) $
    if null sorted
       then putStrLn "# no solutions"
       else putStrLn $ "# best found: " ++ fmtPt (head sorted)

  when (printStats conf) $ do
    printHeader conf
    let ovs = map takeObjectiveValue sorted
    let obest = head ovs
    let omedian = median ovs
    putStrLn $ fmtXs " " [obest, omedian]

  when (dumpAll conf) $ do
    printHeader conf
    -- print the best solution last;
    -- (for scatter-plotting it above the others)
    flip mapM_ (reverse sorted) $ \p -> putStrLn $ fmtPtOneline p
    putStrLn ""

  where

    fmtPt :: (Show a, Real a, PrintfArg a) => Phenotype a -> String
    fmtPt (xs, v) = (printf "%.3g @ [" v) ++ fmtXs ", " xs ++ "]"

    fmtPtOneline :: (Show a, Real a, PrintfArg a) => Phenotype a -> String
    fmtPtOneline p = let xs = map (fromRational.toRational) . takeGenome $ p
                         vs = [takeObjectiveValue p]
                     in  fmtXs " " $ xs ++ vs

    fmtXs :: (Show a, Real a, PrintfArg a) => String -> [a] -> String
    fmtXs sep xs =  intercalate sep $ map (printf "%.3g") xs



-- | Run a genetic algorithm defined by @problemtype@, and @step@.
-- Process command line options to change the number of iterations
-- and logging behaviour.
exampleMain :: (Show a, Real a, PrintfArg a)
            => ExampleDefaults -> ProblemType -> Rand [Genome a] -> StepGA Rand a -> IO ()
exampleMain defaults problemtype initialize step = do

  let options = exampleOptions defaults
  (opts, args, msgs) <- liftM (getOpt Permute options) getArgs
  when (ShowHelp `elem` opts) $ do
    progname <- getProgName
    let header = "usage: " ++ progname ++ " [options]\n\nOptions:\n"
    putStrLn (usageInfo header options)
    exitSuccess

  let conf = updateDefaults defaults opts
  let gens = numGenerations conf
  result <- runGA initialize (loop (Generations gens) step)
  let sorted = bestFirst problemtype $ result
  printSnapshot conf sorted

f :: [Double] -> Double
f [x, y] = (x**2 + y - 11)**2 + (x + y**2 - 7)**2
xvar [x,_] = x
yvar [_,y] = y
g1 [x,y] = 4.84 - (x-0.05)**2 - (y-2.5)**2
g2 [x,y] = x**2 + (y-2.5)**2 - 4.84


constraints = [ 0 .<= xvar <=. 6
              , 0 .<= yvar <=. 6
              , g1 .>=. 0
              , g2 .>=. 0 ]


popsize = 100
initialize = getRandomGenomes popsize [(0,6),(0,6)]
select = withFitnessSharing (distance2 `on` takeGenome) 0.025 1 Minimizing $
         withConstraints constraints (degreeOfViolation 1.0 0.0) Minimizing $
         tournamentSelect Minimizing 2 popsize
step = withFinalDeathPenalty constraints $
       nextGeneration Minimizing f select 0
       (simulatedBinaryCrossover 0.5)
       (gaussianMutate 0.05 0.025)


{-
-- exampleMain takes care of command line options and pretty printing.
-- If you don't need that, a bare bones main function looks like this:
main = do
  results <- runGA initialize (loop (Generations 100) step)
  print . head . bestFirst Minimizing $ results
-}
main = exampleMain (exampleDefaults { numGenerations = 100 } )
       Minimizing initialize step
