let pkgs = (import ./nix/base.nix).pkgs;
    default = import ./default.nix;
in
  default.shellFor {
    withHoogle = true;

    packages = ps: with ps; [homotopic-tech];

    buildInputs = [
      pkgs.texlive.combined.scheme-basic
      pkgs.zlib.out
    ];

    LD_LIBRARY_PATH="${pkgs.zlib.out}/lib";
    exactDeps = true;
  }
